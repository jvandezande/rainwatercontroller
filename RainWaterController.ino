/*
 * This is a simple 2 chennel meter example
 * (c) Z-Wave.Me 2016
 */
 
// channel number
#define ZUNO_CHANNEL_NUMBER_COLD   1

#define PIN_COLD    0

#define PIN_PULSE   13

#define LED_PIN 13

#define DEBOUNCE_COUNT  3
byte cold_triggered = 0;
byte cold_debounce = DEBOUNCE_COUNT;
byte switchstate = 0;

// next macro sets up the Z-Uno channels
// in this example we set up 1 meter channel
// you can read more on http://z-uno.z-wave.me/Reference/ZUNO_SENSOR_MULTILEVEL/
ZUNO_SETUP_CHANNELS(
  ZUNO_SWITCH_BINARY(switchstate, NULL)
);

void setup() 
{
   // Dry contacts of meters connect to these pins
   pinMode(PIN_COLD, INPUT);
   // set LED pin as output
   pinMode(LED_PIN, OUTPUT);      
}

void loop() 
{
    if(digitalRead(PIN_COLD))
    {
        if(!cold_triggered)
        {
          cold_debounce--;
          if(!cold_debounce)
          {
            digitalWrite(LED_PIN, HIGH);   // turn LED on
            switchstate = 255;           
            zunoSendReport(ZUNO_CHANNEL_NUMBER_COLD); 
            cold_debounce = DEBOUNCE_COUNT;
            cold_triggered = true;
          }
        }
    }
    else
    {
      if(cold_triggered)
        {
          cold_debounce--;
          if(!cold_debounce)
          {
            digitalWrite(LED_PIN, LOW);   // turn LED off
            switchstate = 0;
            zunoSendReport(ZUNO_CHANNEL_NUMBER_COLD);
            cold_debounce = DEBOUNCE_COUNT;
            cold_triggered = false;
          }
        }
    }
    delay(100); 
}
